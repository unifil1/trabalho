import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Forca {

private int partida;
private int acertos;
private int erros;
private String palavra;
private ArrayList<String> dicionario;
private Random r = new Random();

    public String sortearPalavra(){
        try{
            File file  = new File("resources/dicionario.txt");
            Scanner sc = new Scanner(file);
            sc.close();
            while (sc.hasNextLine()) {
                dicionario.add(sc.nextLine());
            }
        }catch (FileNotFoundException e) {
            System.out.println(" Ocorreu um erro. Tente novamente.");
            e.printStackTrace();
        }

        int idx = r.nextInt(dicionario.size());
        this.palavra = dicionario.get(idx);
        return mostrarMascara(palavra);
    }

    public String mostrarMascara(String palavra){
        String palavraMask = palavra;
        if (acertos == 0) {
           palavraMask = "_ _ _ _ _";
        }
        System.out.println(palavraMask);
        return palavraMask;
    }

    public static void main(String[] args) {
        Forca f = new Forca();

        System.out.println("Inicio do Jogo");
        System.out.println(" ");
        System.out.println(f.sortearPalavra());
        System.out.println(f.palavra);
    }


}

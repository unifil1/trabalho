import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Jogo {
        private int partida;
        private int acertos = 0;
        private int erros = 0;
        private ArrayList<String> catalogo ;
        private String palavra;
        private String[] palavraMask = {"_", " ", "_", " ", "_", " ", "_", " ", "_"};
        private ArrayList<String> dicionario;
        private Random r = new Random();

        public String sortearPalavra(){
           /* try{
                File file  = new File("C:\\Users\\Amanda\\Desktop\\d.in");
                Scanner sc = new Scanner(file);

                while (sc.hasNextLine()) {
                    this.dicionario.add(sc.nextLine());
                }
                sc.close();
            }catch (FileNotFoundException e) {
                System.out.println(" Ocorreu um erro. Tente novamente.");
                e.printStackTrace();
            }

            int idx = r.nextInt(dicionario.size());
            this.palavra = dicionario.get(idx);*/
           this.palavra = "prova";
            return mostrarMascara("1",99);

        }

        public String mostrarMascara(String letra, int index) {
            if (index == 99) {
                return Arrays.toString(palavraMask);
            } else {

                switch (index) {
                    case 0:
                        palavraMask[0] = letra;
                        break;
                    case 1:
                        palavraMask[2] = letra;
                        break;
                    case 2:
                        palavraMask[4] = letra;
                        break;
                    case 3:
                        palavraMask[6] = letra;
                        break;
                    case 4:
                        palavraMask[8] = letra;
                        break;
                    default:
                        // code block
                }
            }
            return Arrays.toString(palavraMask);
        }

        public void preencher(String letra){
            if (!this.palavra.contains(letra)) {
                setErros();
            } else {
                int index = this.palavra.indexOf(letra);
                mostrarMascara(letra, index);
                setAcertos();
            }
            catalogoLetras(letra);
        }

        public void catalogoLetras(String letra){
            this.catalogo.add(letra);
        }

        public void setAcertos(){
            this.acertos = this.acertos + 1;
        }

        public  void setErros(){
            this.erros = this.erros + 1;
        }

        public int getAcertos(){
            return this.acertos;
        }

        public int getErros(){
            return erros;
        }

        public String getCatalogo(){
            return this.catalogo.toString();
        }

        public String ranking(){
            String ranking = "Total de acertos: "+getAcertos()+'\n'+
                             "Total de erros: "+getErros()+'\n'+
                             "letras utilizadas: "+getCatalogo();
            return ranking;
        }

    }

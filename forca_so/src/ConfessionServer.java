import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ConfessionServer {
    public static void main(String... args) throws IOException {
        ServerSocket endereco = new ServerSocket(16000);
        System.out.println("Aguardando conexão...");

        Socket interlocutor = endereco.accept();
        System.out.println("Conexão aceita");

        PrintWriter sermao = new PrintWriter(interlocutor.getOutputStream());
        sermao.println("Olá! Qual a sua confissão?");
        sermao.flush();

        BufferedReader todoOuvidos = new BufferedReader(new InputStreamReader(interlocutor.getInputStream()));
        String msg = todoOuvidos.readLine();

    while(msg != null){
      if(msg.equals("Bye.")){
        endereco.close();
      }

     System.out.println(msg);
     sermao.println("Olá! Qual a sua confissão?");
     sermao.flush();
     msg = todoOuvidos.readLine();

    }

    }

}

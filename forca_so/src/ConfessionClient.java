import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class ConfessionClient {
    public static void main(String[] args) {
        try{
            Socket confessionSocket = new Socket("127.0.0.1",16000);

            PrintWriter out =  new PrintWriter(confessionSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(confessionSocket.getInputStream())
            );
            BufferedReader stdIn = new BufferedReader(new InputStreamReader (System.in));

            String fromServer;
            String fromUser;

            while ((fromServer = in.readLine())!= null){
                System.out.println("Server: " + fromServer);
                if (fromServer.equals("Bye."))
                    break;
                fromUser = stdIn.readLine();
                if (fromUser != null) {
                    System.out.println("Client: " + fromUser);
                    out.println(fromUser);
                }

            }


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
